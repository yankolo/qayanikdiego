package dawson.com.qayanikdiego.data;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Card is a simple POJO that represents a card. Card objects might be used in many places,
 * for example, in a list that displays all the cards in a specific category or
 * in an activity that displays the details the details of every card.
 *
 * @author Yanik
 */
public class Card implements Serializable {
    private String key;
    private String shortText;
    private String longText;
    private String answer;
    private String dateAdded;
    private String source;

    public Card() {
    }

    public Card(String key, String shortText, String longText, String answer, String dateAdded, String source, String sourceText) {
        this.key = key;
        this.shortText = shortText;
        this.longText = longText;
        this.answer = answer;
        this.dateAdded = dateAdded;
        this.source = source;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getLongText() {
        return longText;
    }

    public void setLongText(String longText) {
        this.longText = longText;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(key, card.key) &&
                Objects.equals(shortText, card.shortText) &&
                Objects.equals(longText, card.longText) &&
                Objects.equals(answer, card.answer) &&
                Objects.equals(dateAdded, card.dateAdded) &&
                Objects.equals(source, card.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, shortText, longText, answer, dateAdded, source);
    }
}
