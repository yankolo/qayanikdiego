package dawson.com.qayanikdiego.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Category;
import dawson.com.qayanikdiego.firebase.FirebaseManager;
import dawson.com.qayanikdiego.ui.adapters.CategoryListAdapter;
import dawson.com.qayanikdiego.util.DualConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The CategoryListActivity displays all the categories in a ListView
 *
 * @author Diego
 */
public class CategoryListActivity extends MenuActivity {
    private static final String TAG = "CATEGORY-LIST-ACTIVITY";
    private final Random random = new Random();

    private CategoryListAdapter categoryListAdapter;
    private ListView categoryListView;

    /**
     * The overridden onCreate method setups the ListView that displays the categories.
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.categorylist_list);
        this.setTitle(R.string.categories);

        categoryListAdapter = new CategoryListAdapter(this, R.layout.categorylist_item);
        categoryListView = this.findViewById(R.id.categoriesList);
        categoryListView.setAdapter(categoryListAdapter);

        setupOnClickListener();

        categoryListAdapter.synchronizeWithFirebase();

        Log.i(TAG, "The category list has been setup");

        final Intent i = this.getIntent();

        if (i.getExtras() != null) {
            Log.i(TAG, "Opened with intent with extra data, resolving what to do...");

            if (i.hasExtra(getString(R.string.open_category))) {
                Log.i(TAG, "Opening the requested category");
                fetchCategories(this::openCategory, i);
            } else if (i.hasExtra(getString(R.string.open_random_category))) {
                Log.i(TAG, "Opening a random category");
                fetchCategories(this::openRandomCategory, i);
            } else {
                Log.i(TAG, "There was nothing to do");
            }
        }
    }

    /**
     * Setups the onItemClickListener of the categoryListView.
     * When a category is pressed, the CardListActivity is launched.
     */
    private void setupOnClickListener() {
        categoryListView.setOnItemClickListener(
                (parent, view, position, id) -> {
                    Category category = categoryListAdapter.getItem(position);

                    Log.i(TAG, "Category '" + category.getName() + "' has been pressed");
                    Log.i(TAG, "Launching CardListActivity");

                    startActivityWithCategory(category);
                });
    }

    /**
     * Starts a {@link CardListActivity} and passes the provided {@link Category} to the new activity
     *
     * @param category The category to start
     */
    private void startActivityWithCategory(Category category) {
        // Creates an intent to start the CardListActivity
        Intent i = new Intent(this, CardListActivity.class);
        i.putExtra(getString(R.string.category), category);
        startActivity(i);
    }

    /**
     * Starts a {@link CardListActivity} and passes the provided {@link Category} & "open_card" key to the new activity
     *
     * @param category The category to start
     * @param card The card key to pass as "open_card"
     */
    private void startActivityWithCategoryAndCard(Category category, String card) {
        // Creates an intent to start the CardListActivity
        Intent i = new Intent(this, CardListActivity.class);
        i.putExtra(getString(R.string.category), category);
        i.putExtra(getString(R.string.open_card), card);
        startActivity(i);
    }

    /**
     * Starts a {@link CardListActivity} and passes the provided {@link Category}
     * & "open_card_random" to the new activity
     *
     * @param category The category to start
     */
    private void startActivityWithCategoryAndRandomCard(Category category) {
        // Creates an intent to start the CardListActivity
        Intent i = new Intent(this, CardListActivity.class);
        i.putExtra(getString(R.string.category), category);
        i.putExtra(getString(R.string.open_random_card), true);
        startActivity(i);
    }

    /**
     * Fetches categories and then calls the provided callback once done
     *
     * @param c The callback to call, will also take in an extra input
     * @param <T> The type
     */
    private <T> void fetchCategories(DualConsumer<T, List<Category>> c, T extra) {
        FirebaseManager.getInstance().getCategoriesDbReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "Got categories!");
                c.accept(extra, FirebaseManager.getInstance().buildCategoryList(dataSnapshot));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "Cancelled while trying to fetch categories", databaseError.toException());
            }
        });
    }

    /**
     * Start a new {@link CardListActivity} with a chosen category
     *
     * @param i The intent used to request the category, used to know if a chosen card should be requested as well
     * @param categories The valid categories that can be picked from
     */
    private void openCategory(Intent i, List<Category> categories) {
        String key = i.getStringExtra(getString(R.string.open_category));
        Category category = null;

        for (Category c : categories) {
            if (c.getKey().equals(key)) {
                category = c;
                break;
            }
        }

        if (category == null) {
            throw new IllegalArgumentException("Did not find the requested category: " + key);
        }

        Log.i(TAG, "Requesting to open category: " + category.getKey());

        if (i.hasExtra(getString(R.string.open_card))) {
            Log.i(TAG, "  Also requesting to open card: " + i.getStringExtra(getString(R.string.open_card)));
            startActivityWithCategoryAndCard(category, i.getStringExtra(getString(R.string.open_card)));
        } else {
            startActivityWithCategory(category);
        }
    }

    /**
     * Start a new {@link CardListActivity} with a random category & request that a random card be chosen as well
     *
     * @param i The intent used to request this action
     * @param categories The categories to pick from
     */
    private void openRandomCategory(Intent i, List<Category> categories) {
        Category category = categories.get(random.nextInt(categories.size()));
        Log.i(TAG, "Requesting to open random category: " + category.getKey());
        startActivityWithCategoryAndRandomCard(category);
    }
}
