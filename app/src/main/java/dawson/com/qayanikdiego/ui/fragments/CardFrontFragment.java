package dawson.com.qayanikdiego.ui.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Card;

/**
 * The CardFrontFragment is the fragment that displays the front of the card
 * (i.e. the question)
 *
 * @author Yanik
 */
public class CardFrontFragment extends Fragment {
    private Card card;

    /**
     * Method that should be used to instantiate the fragment.
     *
     * @param card The card to display
     * @return The CardFrontFragment that represents the front of the card
     */
    public static CardFrontFragment newInstance(Context context, Card card) {
        CardFrontFragment cardFrontFragment = new CardFrontFragment();

        Bundle args = new Bundle();
        args.putSerializable(context.getString(R.string.card), card);
        cardFrontFragment.setArguments(args);

        return cardFrontFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        card = (Card) getArguments().getSerializable(getString(R.string.card));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pagercard_front, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvFront = view.findViewById(R.id.card_front_text);
        tvFront.setText(card.getLongText());

        // Setting the camera distance so that the card will not occupy the whole
        // screen when flipping
        float scale = getResources().getDisplayMetrics().density;
        int cameraDistance = getResources().getInteger(R.integer.card_camera_distance);
        view.setCameraDistance(cameraDistance * scale);
    }
}
