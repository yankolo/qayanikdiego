package dawson.com.qayanikdiego.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import dawson.com.qayanikdiego.util.GlideApp;
import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;

/**
 * The CardBackFragment is the fragment that displays the back of the card
 * (i.e. the answer to the question)
 *
 * @author Yanik
 */
public class CardBackFragment extends Fragment {
    private static final String TAG = "CARD-BACK-FRAGMENT";

    private Card card;
    private Category category;

    /**
     * Method that should be used to instantiate the fragment.
     *
     * @param card     The card to display
     * @param category The category of the card (to display the appropriate image)
     * @return The CardBackFragment that represents the back of the card
     */
    public static CardBackFragment newInstance(Context context, Card card, Category category) {
        CardBackFragment cardBackFragment = new CardBackFragment();

        Bundle args = new Bundle();
        args.putSerializable(context.getString(R.string.card), card);
        args.putSerializable(context.getString(R.string.category), category);
        cardBackFragment.setArguments(args);

        return cardBackFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        card = (Card) getArguments().getSerializable(getString(R.string.card));
        category = (Category) getArguments().getSerializable(getString(R.string.category));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pagercard_back, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvAnswer = view.findViewById(R.id.card_back_answer_text);
        TextView tvDate = view.findViewById(R.id.card_back_date_text);
        Button btnSource = view.findViewById(R.id.card_back_src_btn);
        ImageView ivCategory = view.findViewById(R.id.card_back_category_pic);

        tvAnswer.setText(card.getAnswer());
        tvDate.setText(card.getDateAdded());

        btnSource.setOnClickListener(
                (btnView) -> {
                    Log.i(TAG, "Launching implicit intent to view URL " + card.getSource());

                    Intent implicit = new Intent(Intent.ACTION_VIEW, Uri.parse(card.getSource()));
                    startActivity(implicit);
                }
        );

        // Setting the camera distance so that the card will not occupy the whole
        // screen when flipping
        float scale = getResources().getDisplayMetrics().density;
        int cameraDistance = getResources().getInteger(R.integer.card_camera_distance);
        view.setCameraDistance(cameraDistance * scale);

        loadImage(ivCategory);
    }

    /**
     * Helper method to load the image of a category inside an image view
     *
     * @param imageView The image view where to load the category image
     */
    private void loadImage(ImageView imageView) {
        GlideApp.with(this)
                .load(category.getImage())
                .into(imageView);
    }

}
