package dawson.com.qayanikdiego.ui.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.List;

import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;
import dawson.com.qayanikdiego.ui.fragments.CardContainerFragment;

/**
 * The CardPagerAdapter links a list cards to a ViewPagers.
 * It extends a FragmentStatePagerAdapter for better performance.
 * <p>
 * By only overriding the methods of the FragmentStatePagerAdapter, it is not possible
 * to get the Fragment that is being currently displayed. Therefore, the CardPagerAdapter
 * also overrides methods of the PagerAdapter.
 * <p>
 * It uses a SparseArray to store the Fragments that are currently registered.
 * (Unlike ArrayList, SparseArray permits assignment to array indexes that are
 * beyond the current length of the list)
 * Using this, it is possible to get the currently displayed fragment (by getting
 * the position of the currently displayed view from the ViewPager)
 * <p>
 * Note: In order to flip cards, is should be possible to get the currently displayed fragment
 * <p>
 * Solution presented originally in https://guides.codepath.com/android/ViewPager-with-FragmentPagerAdapter#dynamic-viewpager-fragments
 *
 * @author Yanik
 */
public class CardPagerAdapter extends FragmentStatePagerAdapter {
    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    private List<Card> cards;
    private Category category;
    private Context context;

    public CardPagerAdapter(FragmentManager fragmentManager, List<Card> cards, Category category, Context context) {
        super(fragmentManager);
        this.cards = cards;
        this.category = category;
        this.context = context;
    }

    /**
     * When a fragment is instantiated, add it to the registeredFragments SparseArray
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    /**
     * When a fragment is destroyed, remove it from the registeredFragments SparseArray
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    /**
     * Returns the fragment for the position (if instantiated)
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return CardContainerFragment.newInstance(this.context, cards.get(position), category);
    }

    @Override
    public int getCount() {
        return cards.size();
    }
}
