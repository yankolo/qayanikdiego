package dawson.com.qayanikdiego.ui.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import dawson.com.qayanikdiego.R;

/**
 * The LoginActivity authenticates the user
 *
 * @author Diego
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGIN-ACTIVITY";

    private final static String EMAIL = "firebase@example.com";
    private final static String PASSWORD = "firebase";

    /**
     * The overridden onCreate method makes sure that the user is authenticated. If
     * the user is not authenticated, it will authenticate him. After the user
     * has been authenticated, the CategoryListActivity will be launched.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.login_activity);

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser != null) {
            Log.i(TAG, "User is already authenticated");
            startCategoryListActivity();
        } else {
            Log.i(TAG, "User is not authenticated");
            firebaseAuth.signInWithEmailAndPassword(EMAIL, PASSWORD)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Log.i(TAG, "User has been successfully authenticated");
                            startCategoryListActivity();
                        } else {
                            Log.w(TAG, "User wasn't successfully authenticated");
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setMessage(task.getException().getMessage())
                                    .setTitle(R.string.failed_firebase_login)
                                    .setPositiveButton(android.R.string.ok, null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });
        }
    }

    /**
     * Starts the CategoryListActivity.
     * The intent flags FLAG_ACTIVITY_NEW_TASK and FLAG_ACTIVITY_CLEAR_TASK disallow the user
     * to return to this activity (whose whole purpose is to make sure that the user is authenticated)
     */
    private void startCategoryListActivity() {
        Log.i(TAG, "Launching CategoryListActivity");
        Intent i = new Intent(LoginActivity.this, CategoryListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(i);
    }
}
