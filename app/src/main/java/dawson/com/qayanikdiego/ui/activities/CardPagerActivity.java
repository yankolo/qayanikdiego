package dawson.com.qayanikdiego.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import androidx.viewpager.widget.ViewPager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;
import dawson.com.qayanikdiego.firebase.FirebaseManager;
import dawson.com.qayanikdiego.ui.adapters.CardPagerAdapter;
import dawson.com.qayanikdiego.ui.fragments.CardContainerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * The CardPagerActivity displays a list cards in a ViewPager.
 * It is possible to specify which card to show first by passing an
 * initial_card_key in the intent extras.
 *
 * @author Yanik
 */
public class CardPagerActivity extends MenuActivity {
    private static final String TAG = "CARD-PAGER-ACTIVITY";

    private List<Card> cards = new ArrayList<>();
    private Category category;
    private String initialCardKey;
    private Button flipButton;

    private ViewPager cardViewPager;
    private CardPagerAdapter cardPagerAdapter;

    /**
     * The overridden onCreate method setups the ViewPager to display the cards
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagercard_pager);
        this.setTitle(R.string.card_pager_title);

        loadDataFromExtras();
        findViews();
        setupViewPager();
        loadData();
        setupFlipButton();
    }

    /**
     * Save the last viewed card & category to shared preferences
     */
    @Override
    protected void onStop() {
        super.onStop();

        CardContainerFragment currentFragment
                = ((CardContainerFragment) cardPagerAdapter.getRegisteredFragment(cardViewPager.getCurrentItem()));

        SharedPreferences.Editor editor
                = this.getSharedPreferences(getString(R.string.preference_key_last), MODE_PRIVATE).edit();

        editor.putString(getString(R.string.category), category.getKey());
        editor.putString(getString(R.string.card), currentFragment.getCard().getKey());
        editor.apply();
    }

    /**
     * Helper method to read and load all the data from the intent extras.
     * Loads the initial_card_key -> which card to display first
     * Loads the category -> specifies the category of the card (so that we draw
     * the appropriate image)
     */
    private void loadDataFromExtras() {
        Intent i = getIntent();
        Bundle extraBundle = i.getExtras();

        if (extraBundle == null) {
            throw new IllegalArgumentException("Extras should be passed to the CardPagerActivity. " +
                    "The extras should contain 'initial_card_key' and 'category_key'");
        }

        initialCardKey = i.getExtras().getString(getString(R.string.initial_card_key));
        category = (Category) i.getExtras().getSerializable(getString(R.string.category));
        this.setTitle(category.getName());

        Log.d(TAG, "Data loaded from extras");
    }

    /**
     * Helper method to find all the important views in the layout
     */
    private void findViews() {
        cardViewPager = (ViewPager) findViewById(R.id.card_viewpager);
        flipButton = (Button) findViewById(R.id.flipButton);
    }

    /**
     * Helper method to setup the actual ViewPager.
     * Creates the adapter and links it with the ViewPager
     */
    private void setupViewPager() {
        cardPagerAdapter = new CardPagerAdapter(getSupportFragmentManager(), cards, category, this.getBaseContext());
        cardViewPager.setAdapter(cardPagerAdapter);
        cardViewPager.setCurrentItem(getCardIndex(cards, initialCardKey));

        // Settings necessary to make adjacent pages partially visible
        cardViewPager.setClipToPadding(false);
        cardViewPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.card_pager_space_between_pages_half));

        Log.d(TAG, "ViewPager setup");
    }

    /**
     * Helper method to setup the onClickListener of the FAB (the flip button)
     */
    private void setupFlipButton() {
        flipButton.setOnClickListener(
                (view) -> {
                    Log.d(TAG, "Flip button pressed");
                    int currentCardIndex = cardViewPager.getCurrentItem();
                    CardContainerFragment currentCard = ((CardContainerFragment) cardPagerAdapter.getRegisteredFragment(currentCardIndex));
                    currentCard.flipCard();
                }
        );
    }

    /**
     * Helper method to load the list of cards from the Firebase database
     */
    private void loadData() {
        FirebaseManager firebaseManager = FirebaseManager.getInstance();
        DatabaseReference databaseReference = firebaseManager.getCardListDbReference(category.getKey());
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                cards.addAll(firebaseManager.buildCardList(dataSnapshot));
                setupViewPager();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Value event listener cancelled", databaseError.toException());
            }
        });
    }

    /**
     * Helper method to transform a card key to an index in the
     * card list.
     * It basically searches for the card key in the card list.
     *
     * @param cards The card list to search
     * @param key   The card key to search for
     * @return The index of the card that has the specified key (-1 if not found)
     */
    private int getCardIndex(List<Card> cards, String key) {
        int cardIndex = -1;

        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getKey().equals(key)) {
                cardIndex = i;
                break;
            }
        }

        return cardIndex;
    }
}
