package dawson.com.qayanikdiego.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.database.DatabaseReference;
import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;
import dawson.com.qayanikdiego.firebase.CardListChangeListener;
import dawson.com.qayanikdiego.firebase.FirebaseManager;
import dawson.com.qayanikdiego.ui.activities.CardPagerActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * The CategoryListAdapter links an RecycleView with a list of cards.
 * <p>
 * It is important to note, that the RecycleView that displays the cards, displays
 * only the front preview (a short version of the question). The full version of the card
 * is displayed in the CardPagerActivity.
 *
 * @author Yanik
 */
public class CardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /**
     * The ViewHolder that holds the view of normal items
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFrontPreview;

        public ItemViewHolder(View view) {
            super(view);
            this.tvFrontPreview = (TextView) view.findViewById(R.id.card_front_preview_text);
        }
    }

    /**
     * The ViewHolder that holds the view of the header.
     * To explain, the first item in the list is a header. It is rendered differently
     * then normal items.
     * Having the header inside the list makes it collapse when scrolling.
     */
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHeader;

        public HeaderViewHolder(View view) {
            super(view);
            this.tvHeader = (TextView) view.findViewById(R.id.card_list_header_text);
        }
    }

    private static final String TAG = "CARD-LIST-ADAPTER";

    // Integers used to distinguish the view types (in order to implement header)
    private static final int HEADER_ITEM = 1;
    private static final int NORMAL_ITEM = 2;

    private final Context context;
    private final Category category;
    private final List<Card> cards;

    public CardListAdapter(Context context, Category category) {
        this.context = context;
        this.category = category;
        this.cards = new ArrayList<>();
    }

    /**
     * Gets the list of cards that is contained in the adapter
     *
     * @return The list of cards that is contained in the adapter
     */
    public List<Card> getCards() {
        return cards;
    }

    /**
     * Links the adapter with a CardListChangeListener which will synchronize the list of
     * cards with the Firebase database
     */
    public void synchronizeWithFirebase() {
        DatabaseReference databaseReference = FirebaseManager.getInstance().getCardListDbReference(category.getKey());
        CardListChangeListener cardListListener = new CardListChangeListener(this);

        databaseReference.addChildEventListener(cardListListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder;

        if (viewType == HEADER_ITEM) {
            View cardListHeader = layoutInflater.inflate(R.layout.listcard_header, parent, false);
            return new HeaderViewHolder(cardListHeader);
        } else if (viewType == NORMAL_ITEM) {
            View cardListItem = layoutInflater.inflate(R.layout.listcard_item, parent, false);
            return new ItemViewHolder(cardListItem);
        }

        throw new IllegalArgumentException("Invalid view type used in onCreateViewHolder");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Log.d(TAG, "Binding view for position " + position);

        if (viewHolder instanceof ItemViewHolder) {
            // Decrementing position because there is a header
            // i.e. position 1 should be position 0 in the data list
            position--;

            ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
            Card cardToDisplay = cards.get(position);
            String cardFrontPreview = cardToDisplay.getShortText();

            itemViewHolder.tvFrontPreview.setText(cardFrontPreview);

            View.OnClickListener itemOnClickListener = createItemOnClickListener(viewHolder, position);
            viewHolder.itemView.setOnClickListener(itemOnClickListener);
        }
    }

    private View.OnClickListener createItemOnClickListener(RecyclerView.ViewHolder viewHolder, int position) {
        View.OnClickListener itemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Item at position " + position + " has been pressed");
                Log.i(TAG, "Starting CarPagerActivity");

                Intent i = new Intent(context, CardPagerActivity.class);
                i.putExtra(context.getString(R.string.initial_card_key), cards.get(position).getKey());
                i.putExtra(context.getString(R.string.category), category);

                context.startActivity(i);
            }
        };

        return itemOnClickListener;
    }

    @Override
    public int getItemCount() {
        // The size is +1 because there is a header
        return cards.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return HEADER_ITEM;
        else
            return NORMAL_ITEM;

    }
}
