package dawson.com.qayanikdiego.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import dawson.com.qayanikdiego.firebase.CategoryListChangeListener;
import dawson.com.qayanikdiego.firebase.FirebaseManager;
import dawson.com.qayanikdiego.util.GlideApp;
import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Category;

/**
 * The CategoryListAdapter links an AdapterView with a list of categories.
 *
 * @author Diego
 */
public class CategoryListAdapter extends ArrayAdapter<Category> {
    private static final String TAG = "CATEGORY-LIST-ADAPTER";

    private final LayoutInflater inflater;
    private final int layoutResource;
    private final List<Category> categories;

    public CategoryListAdapter(@NonNull Context context, int resource) {
        this(context, resource, new ArrayList<>());
    }

    public CategoryListAdapter(@NonNull Context context, int resource, List<Category> categories) {
        super(context, resource, categories);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutResource = resource;
        this.categories = categories;
    }

    /**
     * Links the adapter with a CategoryListChangeListener which will synchronize the list of
     * categories with the Firebase database
     */
    public void synchronizeWithFirebase() {
        DatabaseReference databaseReference = FirebaseManager.getInstance().getCategoriesDbReference();
        CategoryListChangeListener categoryListListener = new CategoryListChangeListener(this);

        databaseReference.addChildEventListener(categoryListListener);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Log.d(TAG, "Getting view for position " + position);

        if (convertView == null) {
            convertView = inflater.inflate(layoutResource, parent, false);
        }

        Category category = super.getItem(position);
        TextView tvCategoryName = ((TextView) convertView.findViewById(R.id.category_name));
        tvCategoryName.setText(category.getName());

        // Sets the image inside the image view
        GlideApp
                .with(convertView)
                .load(category.getImage())
                .into((ImageView) convertView.findViewById(R.id.category_image));

        return convertView;
    }

    /**
     * Gets the category list that is contained in the adapter
     *
     * @return The category list that is contained in the adapter
     */
    public List<Category> getCategories() {
        return categories;
    }
}
