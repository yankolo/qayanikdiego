package dawson.com.qayanikdiego.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import dawson.com.qayanikdiego.R;

public class MenuActivity extends AppCompatActivity {

    private final static String TAG = "MENU";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                Log.d(TAG, "Opening up about");
                final Intent i = new Intent(this, AboutActivity.class);
                startActivity(i);
                return true;

            case R.id.random:
                Log.d(TAG, "Opening a random category/card up");
                final Intent iR = new Intent(this, CategoryListActivity.class);
                iR.putExtra(getString(R.string.open_random_category), true);
                // Add these flags so there's no more than 1 CategoryListActivity in the back stack
                iR.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                iR.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(iR);
                return true;

            case R.id.last:
                Log.d(TAG, "Attempting to restore from previous saved category/card");
                SharedPreferences prefs
                        = this.getSharedPreferences(getString(R.string.preference_key_last), MODE_PRIVATE);

                if (!prefs.contains(getString(R.string.category)) || !prefs.contains(getString(R.string.card))) {
                    Toast.makeText(this, R.string.no_last_card, Toast.LENGTH_SHORT).show();
                    return true;
                }

                String category = prefs.getString(getString(R.string.category), "");
                String card = prefs.getString(getString(R.string.card), "");

                final Intent iL = new Intent(this, CategoryListActivity.class);
                iL.putExtra(getString(R.string.open_category), category);
                iL.putExtra(getString(R.string.open_card), card);
                // Add these flags so there's no more than 1 CategoryListActivity in the back stack
                iL.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                iL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(iL);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
