package dawson.com.qayanikdiego.ui.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;

/**
 * The CardContainerFragment contains both CardFrontFragment and CardBackFragment,
 * but displays only one at a time. When changing between the fragments, a flip animation
 * is started.
 * <p>
 * To flip the card, the flipCard() method should be called.
 * <p>
 * CardContainerFragments are used in CardPagerActivity to create a ViewPager
 * of cards.
 *
 * @author Yanik
 */
public class CardContainerFragment extends Fragment {
    private static final String TAG = "CARD-CONTAINER";

    private boolean isFrontShown;
    private Card card;
    private Category category;
    private CardFrontFragment cardFrontFragment;
    private CardBackFragment cardBackFragment;

    /**
     * The method that should be used to instantiate the fragment.
     *
     * @param card The card to display
     * @return The CardContainerFragment that represents the front of the card
     */
    public static CardContainerFragment newInstance(Context context, Card card, Category category) {
        CardContainerFragment cardContainerFragment = new CardContainerFragment();

        Bundle args = new Bundle();
        args.putSerializable(context.getString(R.string.card), card);
        args.putSerializable(context.getString(R.string.category), category);
        cardContainerFragment.setArguments(args);

        return cardContainerFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        card = (Card) getArguments().getSerializable(getString(R.string.card));
        category = (Category) getArguments().getSerializable(getString(R.string.category));
        cardFrontFragment = CardFrontFragment.newInstance(this.requireContext(), card);
        cardBackFragment = CardBackFragment.newInstance(this.requireContext(), card, category);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pagercard_container, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.card_placeholder, cardFrontFragment)
                .commit();

        isFrontShown = true;
    }

    /**
     * Flips the card. If the front is shown, the back is shown (and vice-versa).
     */
    public void flipCard() {
        if (isFrontShown) {
            Log.d(TAG, "Flipping card to show back");
            flipToBack();
        } else {
            Log.d(TAG, "Flipping card to show front");
            flipToFront();
        }
    }

    /**
     * Replaces the card placeholder with the CardFrontFragment.
     * A custom animation, that makes the card flip, is attached to the fragment transaction.
     */
    private void flipToFront() {
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out)
                .replace(R.id.card_placeholder, cardFrontFragment)
                .commit();

        isFrontShown = true;
    }

    /**
     * Replaces the card placeholder with the CardBackFragment.
     * A custom animation, that makes the card flip, is attached to the fragment transaction.
     */
    private void flipToBack() {
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out)
                .replace(R.id.card_placeholder, cardBackFragment)
                .commit();

        isFrontShown = false;
    }

    /**
     * Get the card associated with this fragment
     *
     * @return card
     */
    public Card getCard() {
        return card;
    }
}
