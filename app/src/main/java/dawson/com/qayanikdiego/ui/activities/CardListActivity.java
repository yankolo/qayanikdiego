package dawson.com.qayanikdiego.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import dawson.com.qayanikdiego.R;
import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;
import dawson.com.qayanikdiego.firebase.FirebaseManager;
import dawson.com.qayanikdiego.ui.adapters.CardListAdapter;
import dawson.com.qayanikdiego.util.DualConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This CardListActivity displays a list of cards in a specified category.
 * <p>
 * Instead of displaying the whole front of the card, only the front preview is
 * shown (the short version of the question). The full version of the cards
 * is displayed in the CardPagerActivity.
 * <p>
 * The list is implemented using a RecyclerView and its adapter is CardListAdapter.
 *
 * @author Yanik
 */
public class CardListActivity extends MenuActivity {

    private final static String TAG = "CARD-LIST-ACTIVITY";
    private final Random random = new Random();

    /**
     * The overridden onCreate method setups the RecyclerView to display the cards
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listcard_list);

        final Intent i = this.getIntent();

        if (i.getExtras() == null) {
            throw new IllegalArgumentException("This activity should be started with an intent that"
                    + "specifies what category to use");
        }

        Category category = (Category) i.getExtras().getSerializable(getString(R.string.category));
        this.setTitle(category.getName());

        CardListAdapter cardListAdapter = new CardListAdapter(this, category);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());

        RecyclerView cardListRecycler = findViewById(R.id.card_recycler_view);
        cardListRecycler.setAdapter(cardListAdapter);
        cardListRecycler.setLayoutManager(layoutManager);
        cardListRecycler.addItemDecoration(dividerItemDecoration);

        cardListAdapter.synchronizeWithFirebase();

        if (i.getExtras() != null) {
            Log.i(TAG, "Opened with intent with extra data, resolving what to do...");

            if (i.hasExtra(getString(R.string.open_card))) {
                Log.i(TAG, "Opening the requested card");
                fetchCards(this::openCard, i);
            } else if (i.hasExtra(getString(R.string.open_random_card))) {
                Log.i(TAG, "Opening a random card");
                fetchCards(this::openRandomCard, i);
            } else {
                Log.i(TAG, "There was nothing to do");
            }
        }
    }

    /**
     * Fetches cards and then calls the provided callback once done
     *
     * @param c   The callback to call, will also take in an extra input
     * @param extra The intent used to create this activity
     */
    private void fetchCards(DualConsumer<Intent, List<Card>> c, Intent extra) {
        final String categoryKey = ((Category) extra.getExtras().getSerializable(getString(R.string.category))).getKey();

        FirebaseManager.getInstance().getCardListDbReference(categoryKey)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d(TAG, "Got cards for: " + categoryKey);
                        c.accept(extra, FirebaseManager.getInstance().buildCardList(dataSnapshot));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.w(TAG, "Cancelled while trying to fetch cards", databaseError.toException());
                    }
                });
    }

    /**
     * Start a new {@link CardPagerActivity} with a chosen card
     *
     * @param i The intent used to request the category, used to know if a chosen card should be requested as well
     * @param cards The valid cards that can be picked from
     */
    private void openCard(Intent i, List<Card> cards) {
        String key = i.getStringExtra(getString(R.string.open_card));
        Card card = null;

        for (Card c : cards) {
            if (c.getKey().equals(key)) {
                card = c;
                break;
            }
        }

        if (card == null) {
            throw new IllegalArgumentException("Did not find the requested card: " + key);
        }

        Log.i(TAG, "Requesting to open card: " + card.getKey());

        Intent cardIntent = new Intent(this, CardPagerActivity.class);
        cardIntent.putExtra(getString(R.string.initial_card_key), card.getKey());
        cardIntent.putExtra(getString(R.string.category), i.getSerializableExtra(getString(R.string.category)));

        this.startActivity(cardIntent);
    }

    /**
     * Start a new {@link CardPagerActivity} with a random card
     *
     * @param i The intent used to request this action
     * @param cards The cards to pick from
     */
    private void openRandomCard(Intent i, List<Card> cards) {
        Card card = cards.get(random.nextInt(cards.size()));
        Log.i(TAG, "Requesting to open random category: " + card.getKey());

        Intent cardIntent = new Intent(this, CardPagerActivity.class);
        cardIntent.putExtra(getString(R.string.initial_card_key), card.getKey());
        cardIntent.putExtra(getString(R.string.category), i.getSerializableExtra(getString(R.string.category)));

        this.startActivity(cardIntent);
    }
}
