package dawson.com.qayanikdiego.firebase;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.List;

import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.ui.adapters.CardListAdapter;

/**
 * The CardListChangeListener is a class that syncs a card list in a CardListAdapter
 * with the Firebase database. It implements the ChildEventListener callbacks
 * (which are called when there is change in a node's children). Every callback
 * implemented modifies the card list differently. When the callbacks modify the card
 * list, they notify the adapter so that the adapter knows how to update the
 * RecyclerView.
 * <p>
 * To use the class, an instance of the CardListChangeListener needs to be attached
 * to a database reference that points to a node that holds a list of cards
 * (using ChildEventListener()).
 *
 * @author Yanik
 */
public class CardListChangeListener implements ChildEventListener {
    private static final String TAG = "CARD-LIST-LISTENER";

    private final FirebaseManager firebaseManager;
    private final CardListAdapter cardListAdapter;
    private final List<Card> cards;

    public CardListChangeListener(CardListAdapter cardListAdapter) {
        this.firebaseManager = FirebaseManager.getInstance();
        this.cardListAdapter = cardListAdapter;
        this.cards = cardListAdapter.getCards();
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildKey) {
        Log.d(TAG, "Child added - previousChildKey -> '" + previousChildKey + "'");

        Card card = firebaseManager.buildCard(dataSnapshot);

        int insertionIndex = -1;

        if (previousChildKey != null) {
            int previousChildIndex = -1;

            for (Card current : cards) {
                if (current.getKey().equals(previousChildKey)) {
                    previousChildIndex = cards.indexOf(current);
                    break;
                }
            }

            insertionIndex = previousChildIndex + 1;
        }

        if (insertionIndex < 0) {
            cards.add(card);
        } else {
            cards.add(insertionIndex, card);
        }

        cardListAdapter.notifyItemInserted(insertionIndex);
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildKey) {
        Log.d(TAG, "Child changed - previousChildKey -> '" + previousChildKey + "'");

        Card card = firebaseManager.buildCard(dataSnapshot);

        int cardIndex = -1;

        if (previousChildKey != null) {
            int previousChildIndex = -1;

            for (Card current : cards) {
                if (current.getKey().equals(previousChildKey)) {
                    previousChildIndex = cards.indexOf(current);
                    break;
                }
            }

            cardIndex = previousChildIndex + 1;
        }

        cards.set(cardIndex, card);
        cardListAdapter.notifyItemChanged(cardIndex);
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Log.d(TAG, "Child removed - key -> '" + dataSnapshot.getKey() + "'");

        String cardKey = dataSnapshot.getKey();

        int deletedCardIndex = -1;

        for (Card current : cards) {
            if (current.getKey().equals(cardKey)) {
                deletedCardIndex = cards.indexOf(current);
                break;
            }
        }

        cards.remove(deletedCardIndex);
        cardListAdapter.notifyItemRemoved(deletedCardIndex);
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildKey) {
        Log.d(TAG, "Child Moved - previousChildKey -> '" + previousChildKey + "'");

        Card card = firebaseManager.buildCard(dataSnapshot);

        int newLocationIndex = -1;
        int oldLocationIndex = -1;

        for (Card current : cards) {
            if (current.getKey().equals(dataSnapshot.getKey())) {
                oldLocationIndex = cards.indexOf(current);
                break;
            }
        }

        cards.remove(oldLocationIndex);

        int previousChildIndex = -1;

        if (previousChildKey != null) {
            for (Card current : cards) {
                if (current.getKey().equals(previousChildKey)) {
                    previousChildIndex = cards.indexOf(current);
                    break;
                }
            }
        }

        newLocationIndex = previousChildIndex + 1;
        cards.add(newLocationIndex, card);
        cardListAdapter.notifyItemMoved(oldLocationIndex, newLocationIndex);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w(TAG, "Card List child event listener cancelled", databaseError.toException());
    }
}
