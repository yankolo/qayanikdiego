package dawson.com.qayanikdiego.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dawson.com.qayanikdiego.data.Card;
import dawson.com.qayanikdiego.data.Category;

/**
 * The FirebaseManager is general class that makes the interaction with the Firebase API easier.
 * The class might be used throughout the whole application, which makes
 * it a good candidate for the Singleton pattern.
 * <p>
 * A Singleton is used (instead of static methods) as in the future the
 * FirebaseManager might contain some metadata about the Firebase connection
 * (e.g. logged in user information) and having a single object containing this
 * information makes it easier to manage it.
 * <p>
 * To get the only instance of the FirebaseManager use the getInstance() method.
 *
 * @author Yanik
 */
public class FirebaseManager {
    // FirebaseManager is a Singleton (only one instance can exist)
    private static FirebaseManager firebaseManager;

    /**
     * Private constructor makes it so that the only way to instantiate the object
     * is using the getInstance() method
     */
    private FirebaseManager() {
        // Prevent access to constructor using Reflection
        if (firebaseManager != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    /**
     * The method that is used to get the single instance of a FirebaseManager.
     * Lazily instantiates the object.
     *
     * @return The single instance of a FirebaseManager
     */
    public synchronized static FirebaseManager getInstance() {
        if (firebaseManager == null) {
            firebaseManager = new FirebaseManager();
        }

        return firebaseManager;
    }

    /**
     * Gets a Firebase database reference that points to node that
     * contains all the categories (without the cards)
     *
     * @return A Firebase database reference that points to node that
     * contains all the categories
     */
    public DatabaseReference getCategoriesDbReference() {
        return FirebaseDatabase.getInstance().getReference().child("categories");
    }

    /**
     * Gets a Firebase database reference that points to a node that
     * contains a map of cards.
     *
     * @param categoryKey The category of the cards
     * @return Firebase database reference that points to a node that
     * contains a map of cards.
     */
    public DatabaseReference getCardListDbReference(String categoryKey) {
        return FirebaseDatabase.getInstance().getReference().child("questions").child(categoryKey);
    }

    /**
     * Creates a category object from a DataSnapshot.
     *
     * @param categoryDataSnapshot A DataSnapshot that contains a category
     * @return The category that was constructed from the DataSnapshot
     */
    public Category buildCategory(DataSnapshot categoryDataSnapshot) {
        Category category = categoryDataSnapshot.getValue(Category.class);
        category.setKey(categoryDataSnapshot.getKey());

        return category;
    }

    /**
     * Creates a Category List from a DataSnapshot
     *
     * @param categoryListDataSnapshot A DataSnapshot that contains a list of categories
     * @return A list of categories that was constructed from the DataSnapshot
     */
    public List<Category> buildCategoryList(DataSnapshot categoryListDataSnapshot) {
        List<Category> categoryList = new ArrayList<>();

        for (DataSnapshot categoryDataSnapshot : categoryListDataSnapshot.getChildren()) {
            categoryList.add(buildCategory(categoryDataSnapshot));
        }

        return categoryList;
    }

    /**
     * Creates a Card List from a DataSnapshot
     *
     * @param cardListDataSnapshot A DataSnapshot that contains a list of cards
     * @return A list of cards that was constructed from the DataSnapshot
     */
    public List<Card> buildCardList(DataSnapshot cardListDataSnapshot) {
        List<Card> cardList = new ArrayList<>();

        for (DataSnapshot cardDataSnapshot : cardListDataSnapshot.getChildren()) {
            cardList.add(buildCard(cardDataSnapshot));
        }

        return cardList;
    }

    /**
     * Creates a Card from a DataSnapshot
     *
     * @param cardDataSnapshot A DataSnapshot that contains a card
     * @return A Card that was constructed from the DataSnapshot
     */
    public Card buildCard(DataSnapshot cardDataSnapshot) {
        Map<String, Object> jsonCard = (Map<String, Object>) cardDataSnapshot.getValue();
        String key = cardDataSnapshot.getKey();

        return buildCardFromMap(key, jsonCard);
    }

    /**
     * Helper method to create a Card from a Map
     *
     * @param key     The key of the card
     * @param cardMap A Map that contains a Card
     * @return A card constructed from the Map
     */
    private Card buildCardFromMap(String key, Map<String, Object> cardMap) {
        Object frontPreview = cardMap.get("short");
        Object front = cardMap.get("long");
        Object back = cardMap.get("answer");
        Object dateAdded = cardMap.get("data");
        Object sourceURL = cardMap.get("source");

        Card card = new Card();

        card.setKey(key);
        card.setShortText(frontPreview != null ? frontPreview.toString() : "");
        card.setLongText(front != null ? front.toString() : "");
        card.setAnswer(back != null ? back.toString() : "");
        card.setDateAdded(dateAdded != null ? dateAdded.toString() : "");
        card.setSource(sourceURL != null ? sourceURL.toString() : "");

        return card;
    }
}
