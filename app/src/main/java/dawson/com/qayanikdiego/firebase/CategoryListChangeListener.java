package dawson.com.qayanikdiego.firebase;

import android.util.Log;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import dawson.com.qayanikdiego.data.Category;
import dawson.com.qayanikdiego.ui.adapters.CategoryListAdapter;

/**
 * The CategoryListChangeListener is a class that syncs a category list in a CategoryListAdapter
 * with the Firebase database. It implements the ChildEventListener callbacks
 * (which are called when there is change in a node's children). Every callback
 * implemented modifies the category list differently. When the callbacks modify the category
 * list, they notify the adapter.
 * <p>
 * To use the class, an instance of the CategoryListChangeListener needs to be attached
 * to a database reference that points to a node that holds a list of categories
 * (using ChildEventListener()).
 *
 * @author Diego
 */
public class CategoryListChangeListener implements ChildEventListener {
    private static final String TAG = "CATEGORY-LIST-LISTENER";

    private final FirebaseManager firebaseManager;
    private final CategoryListAdapter categoryListAdapter;

    public CategoryListChangeListener(CategoryListAdapter categoryListAdapter) {
        this.firebaseManager = FirebaseManager.getInstance();
        this.categoryListAdapter = categoryListAdapter;
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildKey) {
        Log.d(TAG, "Child added - previousChildKey -> '" + previousChildKey + "'");

        Category category = firebaseManager.buildCategory(dataSnapshot);

        int insertionIndex = -1;

        if (previousChildKey != null) {
            final Category previousChild = new Category();
            previousChild.setKey(previousChildKey);
            insertionIndex = categoryListAdapter.getPosition(previousChild) + 1;
        }

        categoryListAdapter.insert(category, (insertionIndex < 0 ? 0 : insertionIndex));
        categoryListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildKey) {
        Log.d(TAG, "Child changed - previousChildKey -> '" + previousChildKey + "'");

        Category category = firebaseManager.buildCategory(dataSnapshot);

        final int categoryIndex = categoryListAdapter.getPosition(category);

        categoryListAdapter.remove(category);
        categoryListAdapter.insert(category, categoryIndex);
        categoryListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Log.d(TAG, "Child removed - key -> '" + dataSnapshot.getKey() + "'");

        Category category = firebaseManager.buildCategory(dataSnapshot);
        categoryListAdapter.remove(category);
        categoryListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildKey) {
        Log.d(TAG, "Child Moved - previousChildKey -> '" + previousChildKey + "'");

        Category category = firebaseManager.buildCategory(dataSnapshot);

        categoryListAdapter.remove(category);

        int insertionIndex = -1;

        if (previousChildKey != null) {
            final Category previousChild = new Category();
            previousChild.setKey(previousChildKey);
            insertionIndex = categoryListAdapter.getPosition(previousChild) + 1;
        }

        categoryListAdapter.insert(category, (insertionIndex < 0 ? 0 : insertionIndex));
        categoryListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w(TAG, "Category List child event listener cancelled", databaseError.toException());
    }
}
