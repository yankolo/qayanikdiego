package dawson.com.qayanikdiego.util;

/**
 * A class that works exactly like a BiConsumer, except BiConsumer is API v24+ only so here's another implementation :(
 *
 * @param <T> The first argument type
 * @param <U> The second argument type
 * @author Diego Plaza-Ottereyes
 */
public interface DualConsumer<T, U> {
    void accept(T arg1, U arg2);
}
